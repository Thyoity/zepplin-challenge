# Zepplin Challenge - Front-end Vue.js

Bem-vindo ao **Zepplin Challenge - Front-end**, vamos analisar seu conhecimento em *Vue.js*.  
Para começar faça um Fork desse projeto e crie uma branch com seu nome, ou crie um novo repositório no Github ou Gitlab e mande o link para nós :D

#### Prazo de entrega
- 6 dias contando com o dia do início do desafio

#### Vamos avaliar...
- Qualidade do seu código
- Boas práticas
- Conhecimento em Javascript moderno
- HTML e CSS
- Conhecimento em Vue.js

#### Você deve...
- Documentar como rodar o projeto desenvolvido
- Configurar o projeto inicial do Vue.js
- Utilizar as endpoints da API fornecida
- Desenvolver a interface com base nas imagens do layout presentes mais abaixo
- Usar o Inglês na programação
  
#### Seria interessante...
- Algum tratamento de erro caso retorne erro na requisição
- TypeScript

---
# Instruções

O desafio consiste em criar um painel com autenticação, e um CRUD de produtos utilizando a API do Zepplin.

*Link da API: ```https://challenge-api.zepplin.com.br```  
*Nas rotas de criação, atualização e exclusão de produtos e categorias devem ser passadas um TOKEN de autenticação pelos Headers da requisição
```
headers: {
    authorization: 'Bearer {{TOKEN_AQUI}}'
}
```
---
Para obter esse token de autenticação, é necessário fazer login no Zepplin utilizando o endpoint a seguir:
### POST ```/auth/master-users```
*É necessário que sejam utilizados o email e senha abaixo
```
// body
{
    "email": "challenge@zepplin.com.br",
    "password": "z/e$p8pLki2n"
}

```  
Será retornado um token caso dê sucesso, esse token deve ser armazenado para ser utilizado nas outras requisições
##### Interface de login
![Interface de login](https://i.imgur.com/DTmomSl.jpg)

---

### GET ```/categories```  
*Não é necessário autenticação  
Esse endpoint retorna todas as categorias

---

### GET ```/products```
*Não é necessário autenticação  
Retorna todos os produtos, incluindo o id da categoria em que o produto está associado

**Os produtos com ```status: 'inactive'``` deverão aparecer na lista com opacidade reduzido
![Interface lista de produtos](https://i.imgur.com/Grznxbw.jpg)

Para os formulários de edição e criação dos produtos, pode-se usar tanto um modal, quanto poderá utilizar uma rota a parte do front-end para essa finalidade. O layout fica a critério do desafiante.

---

### POST ```/products```
*É necessário autenticação  
É obrigatório que o produto tenha um ```categoryId```
```
//body
{
    "name": "Queijo", (obrigatório)
    "categoryId": "5e9f392cceaa137252787cdd" (obrigatório)
}
```

A tela de adição e edição de um produto precisa ter um component de **select** que lista todas as categorias
para poder selecionar a categoria em que aquele produto pertence (apenas as categorias com ```status: 'active'``` poderam ser selecionadas)

---

### PATCH ```/products/{{PRODUCT_ID}}```
*É necessário autenticação

```
//body
{
    "name": "Pastel",
    "status": "inactive" | "active"
}
```

Deverá ser seguido o mesmo padrão da tela de adição.  
Deverá ser possível atualizar o nome, categoria associada (select) e status do produto

### DELETE ```/products/{{PRODUCT_ID}}```
*É necessário autenticação

